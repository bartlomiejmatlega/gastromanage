class OrderSerializer < ActiveModel::Serializer
  attributes :id, :comment, :total_price, :delivery_method, :payment_method, :process_status

  belongs_to :client, serializer: ClientForOrderSerializer
  has_many :line_items, serializer: LineItemForOrderSerializer
end
