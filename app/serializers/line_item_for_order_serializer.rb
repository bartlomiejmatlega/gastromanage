class LineItemForOrderSerializer < ActiveModel::Serializer
  attributes :id, :product_id, :price, :line_item_additionals
end
