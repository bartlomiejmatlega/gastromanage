class ProductBundle < Product
  has_many :product_combinations, foreign_key: :bundle_id
  has_many :units, through: :product_combinations
end
