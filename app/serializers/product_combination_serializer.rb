class ProductCombinationSerializer < ActiveModel::Serializer
  attributes :id, :count, :unit_id, :bundle_id
end
