Rails.application.routes.draw do
  namespace :api do
    resources :line_item_additionals
    resources :line_items
    resources :products
    resources :product_units
    resources :product_bundles
    resources :product_combinations
    resources :orders
    resources :clients
  end

  apipie
end
