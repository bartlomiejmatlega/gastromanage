class AddCountToProductCombination < ActiveRecord::Migration[5.0]
  def change
    add_column :product_combinations, :count, :integer, default: 1
  end
end
