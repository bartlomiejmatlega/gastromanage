class Api::ProductUnitsController < ApplicationController
  before_action :set_product_unit, only: [:show, :update, :destroy]

  # GET /product_units
  api :GET, "/product_units", "Show all product_units"
  def index
    @product_units = ProductUnit.all
    render json: @product_units
  end

  # GET /product_units/1
  api :GET, "/product_units/:id", "Show product with id :id"
  param :id, :number, desc: "ID of product", required: true
  def show
    render json: @product_unit
  end

  # POST /product_units
  api :POST, "/product_units", "Create new product unit"
  param :product_unit, Hash, desc: "Hash array with all params" do
    param :name, String, desc: "ID of client", required: true
    param :price, :number, desc: "Price of product in grosze", required: true
  end
  def create
    @product_unit = ProductUnit.new(product_params)

    if @product_unit.save
      render json: @product_unit, status: :created, location: [:api, @product_unit]
    else
      render json: @product_unit.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_units/1
  api :PUT, "/product_units/:id", "Update product unit with id :id"
  param :id, :number, desc: "ID of product", required: true
  param :product_unit, Hash, desc: "Hash array with all params" do
    param :name, String, desc: "ID of client"
    param :price, :number, desc: "Price of product in grosze"
  end
  def update
    if @product_unit.update(product_params)
      render json: @product_unit
    else
      render json: @product_unit.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_units/1
  api :DELETE, "/product_units/:id", "Delete product unit with id :id"
  param :id, :number, desc: "ID of product", required: true
  def destroy
    @product_unit.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_unit
      @product_unit = ProductUnit.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product_unit).permit(:name, :price)
    end
end
