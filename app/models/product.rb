class Product < ApplicationRecord
  has_many :line_items
  has_many :line_item_additionals

  validates :type, presence: :true
end
