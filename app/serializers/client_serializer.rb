class ClientSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :address, :zip_code, :city

  has_many :orders
end
