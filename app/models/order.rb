class Order < ApplicationRecord
  belongs_to :client

  has_many :line_items
  has_many :line_items_additionals, through: :line_items
end
