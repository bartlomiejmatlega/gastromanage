require 'test_helper'

class LineItemAdditionalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @line_item_additional = line_item_additionals(:one)
  end

  test "should get index" do
    get line_item_additionals_url, as: :json
    assert_response :success
  end

  test "should create line_item_additional" do
    assert_difference('LineItemAdditional.count') do
      post line_item_additionals_url, params: { line_item_additional: { line_item_id: @line_item_additional.line_item_id, product_id: @line_item_additional.product_id } }, as: :json
    end

    assert_response 201
  end

  test "should show line_item_additional" do
    get line_item_additional_url(@line_item_additional), as: :json
    assert_response :success
  end

  test "should update line_item_additional" do
    patch line_item_additional_url(@line_item_additional), params: { line_item_additional: { line_item_id: @line_item_additional.line_item_id, product_id: @line_item_additional.product_id } }, as: :json
    assert_response 200
  end

  test "should destroy line_item_additional" do
    assert_difference('LineItemAdditional.count', -1) do
      delete line_item_additional_url(@line_item_additional), as: :json
    end

    assert_response 204
  end
end
