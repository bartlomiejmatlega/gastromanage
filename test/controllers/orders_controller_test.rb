require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @order = orders(:one)
  end

  test "should get index" do
    get orders_url, as: :json
    assert_response :success
  end

  test "should create order" do
    assert_difference('Order.count') do
      post orders_url, params: { order: { client_id: @order.client_id, comment: @order.comment, delivery_method: @order.delivery_method, payment_method: @order.payment_method, process_status: @order.process_status, total_price: @order.total_price } }, as: :json
    end

    assert_response 201
  end

  test "should show order" do
    get order_url(@order), as: :json
    assert_response :success
  end

  test "should update order" do
    patch order_url(@order), params: { order: { client_id: @order.client_id, comment: @order.comment, delivery_method: @order.delivery_method, payment_method: @order.payment_method, process_status: @order.process_status, total_price: @order.total_price } }, as: :json
    assert_response 200
  end

  test "should destroy order" do
    assert_difference('Order.count', -1) do
      delete order_url(@order), as: :json
    end

    assert_response 204
  end
end
