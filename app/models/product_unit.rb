class ProductUnit < Product
  has_many :product_combinations, foreign_key: :unit_id
  has_many :bundles, through: :product_combinations
end
