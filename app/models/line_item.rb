class LineItem < ApplicationRecord
  belongs_to :order
  belongs_to :product

  has_many :line_item_additionals
end
