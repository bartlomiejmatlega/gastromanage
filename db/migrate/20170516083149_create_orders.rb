class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.references :client, foreign_key: true
      t.integer :total_price
      t.string :comment
      t.integer :delivery_method
      t.integer :payment_method
      t.integer :process_status

      t.timestamps
    end
  end
end
