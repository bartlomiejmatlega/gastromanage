class LineItemAdditionalSerializer < ActiveModel::Serializer
  attributes :id, :order, :product, :line_item
end
