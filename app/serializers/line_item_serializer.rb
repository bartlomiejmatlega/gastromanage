class LineItemSerializer < ActiveModel::Serializer
  attributes :id, :order, :product, :price, :line_item_additionals
end
