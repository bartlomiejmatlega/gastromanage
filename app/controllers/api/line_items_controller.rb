class Api::LineItemsController < ApplicationController
  before_action :set_line_item, only: [:show, :update, :destroy]

  # GET /line_items
  api :GET, "/line_items", "Show all line items"
  def index
    @line_items = LineItem.all

    render json: @line_items
  end

  # GET /line_items/1
  api :GET, "/line_items/:id", "Show line items with id :id"
  param :id, :number, desc: "ID of line items", required: true
  def show
    render json: @line_item
  end

  # POST /line_items
  api :POST, "/line_item", "Create new line item"
  param :line_item, Hash, desc: "Hash array with all params", required: true do
    param :order_id, :number, desc: "ID of order", required: true
    param :product_id, :number, desc: "ID of product", required: true
  end
  def create
    @line_item = LineItem.new(line_item_params)

    if @line_item.save
      render json: @line_item, status: :created, location: [:api, @line_item]
    else
      render json: @line_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /line_items/1
  api :PUT, "/line_item_additionals/:id", "Update additional line item with id :id"
  param :id, :number, desc: "ID of line item", required: true
  param :line_item, Hash, desc: "Hash array with all params", required: true do
    param :order_id, :number, desc: "ID of order"
    param :product_id, :number, desc: "ID of product"
  end
  def update
    if @line_item.update(line_item_params)
      render json: @line_item
    else
      render json: @line_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /line_items/1
  api :DELETE, "/line_items/:id", "Delete line item with id :id"
  param :id, :number, desc: "ID of line item", required: true
  def destroy
    @line_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item
      @line_item = LineItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def line_item_params
      params.require(:line_item).permit(:order_id, :product_id, :price)
    end
end
