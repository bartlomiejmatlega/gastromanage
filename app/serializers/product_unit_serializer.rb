class ProductUnitSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :type
end
