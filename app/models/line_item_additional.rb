class LineItemAdditional < ApplicationRecord
  belongs_to :line_item
  belongs_to :product
  delegate :order, to: :line_item
end
