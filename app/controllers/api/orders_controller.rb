class Api::OrdersController < ApplicationController
  before_action :set_order, only: [:show, :update, :destroy]

  # GET /orders
  api :GET, "/orders", "Show all orders"
  def index
    @orders = Order.all

    render json: @orders
  end

  # GET /orders/1
  api :GET, "/orders/:id", "Show order with id :id"
  param :id, :number, desc: "ID of order", required: true
  def show
    render json: @order
  end

  # POST /orders
  api :POST, "/orders", "Create new order"
  param :order, Hash, desc: "Hash array with all params" do
    param :client_id, :number, desc: "ID of client", required: true
    param :comment, String, desc: "Comment for order"
    param :delivery_method, :number, desc: "Method of delivery [1 - 3]", required: true
    param :payment_method, :number, desc: "Method of payment [1 - 3]", required: true
    param :process_status, :number, desc: "Status of process [1 - 3]", required: true
  end
  def create
    @order = Order.new(order_params)

    if @order.save
      render json: @order, status: :created, location: [:api, @order]
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  api :PUT, "/orders/:id", "Update order with id :id"
  param :id, :number, desc: "ID of order", required: true
  param :order, Hash, desc: "Hash array with all params" do
    param :client_id, :number, desc: "ID of client"
    param :comment, String, desc: "Comment for order"
    param :delivery_method, :number, desc: "Method of delivery [1 - 3]"
    param :payment_method, :number, desc: "Method of payment [1 - 3]"
    param :process_status, :number, desc: "Status of process [1 - 3]"
  end
  def update
    if @order.update(order_params)
      render json: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  api :DELETE, "/orders/:id", "Delete order with id :id"
  param :id, :number, desc: "ID of order", required: true
  def destroy
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def order_params
      params.require(:order).permit(:client_id, :total_price, :comment, :delivery_method, :payment_method, :process_status)
    end
end
