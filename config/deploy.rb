lock "3.8.1"

set :application, "gastro_manage"
set :repo_url, "git@gitlab.com:bartlomiejmatlega/gastromanage.git"
set :deploy_to, '/home/deploy/gastro_manage'

append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "vendor/bundle", "public/system", "public/uploads"
