class ProductBundleSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :type, :units
end
