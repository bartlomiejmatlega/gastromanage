class Api::ProductsController < ApplicationController
  before_action :set_product, only: [:show, :update, :destroy]

  # GET /products
  api :GET, "/products", "Show all products"
  def index
    @products = Product.all
    render json: @products
  end

  # GET /products/1
  api :GET, "/products/:id", "Show product with id :id"
  param :id, :number, desc: "ID of product", required: true
  def show
    render json: @product
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product).permit(:name, :price)
    end
end
