class Api::ProductCombinationsController < ApplicationController
  before_action :set_product_combination, only: [:show, :update, :destroy]

  # GET /product_combinations
  api :GET, "/product_combinations", "Show all product_combinations"
  def index
    @product_combinations = ProductCombination.all
    render json: @product_combinations
  end

  # GET /product_combinations/1
  api :GET, "/product_combinations/:id", "Show product with id :id"
  param :id, :number, desc: "ID of product", required: true
  def show
    render json: @product_combination
  end

  # POST /product_combinations
  api :POST, "/product_combinations", "Create new product combination"
  param :product_combination, Hash, desc: "Hash array with all params" do
    param :bundle_id, :number, desc: "ID of product bundle", required: true
    param :unit_id, :number, desc: "ID of product unit", required: true
    param :count, :number, desc: "Count of units in bundle [default 1]"
  end
  def create
    @product_combination = ProductCombination.new(product_params)

    if @product_combination.save
      render json: @product_combination, status: :created, location: [:api, @product_combination]
    else
      render json: @product_combination.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_combinations/1
  api :PUT, "/product_combinations/:id", "Update product combination with id :id"
  param :id, :number, desc: "ID of product combination", required: true
  param :product_combination, Hash, desc: "Hash array with all params" do
    param :bundle_id, :number, desc: "ID of product bundle"
    param :product_id, :number, desc: "ID of product unit"
    param :count, :number, desc: "Count of units in bundle [default 1]"
  end
  def update
    if @product_combination.update(product_params)
      render json: @product_combination
    else
      render json: @product_combination.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_combinations/1
  api :DELETE, "/product_combinations/:id", "Delete product combination with id :id"
  param :id, :number, desc: "ID of product", required: true
  def destroy
    @product_combination.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_combination
      @product_combination = ProductCombination.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product_combination).permit(:unit_id, :bundle_id, :count)
    end
end
