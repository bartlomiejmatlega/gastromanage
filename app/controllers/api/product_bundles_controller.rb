class Api::ProductBundlesController < ApplicationController
  before_action :set_product_bundle, only: [:show, :update, :destroy]

  # GET /product_bundles
  api :GET, "/product_bundles", "Show all product_bundles"
  def index
    @product_bundles = ProductBundle.all
    render json: @product_bundles
  end

  # GET /product_bundles/1
  api :GET, "/product_bundles/:id", "Show product with id :id"
  param :id, :number, desc: "ID of product", required: true
  def show
    render json: @product_bundle
  end

  # POST /product_bundles
  api :POST, "/product_bundles", "Create new product bundle"
  param :product_bundle, Hash, desc: "Hash array with all params" do
    param :name, String, desc: "ID of client", required: true
    param :price, :number, desc: "Price of product in grosze", required: true
  end
  def create
    @product_bundle = ProductBundle.new(product_params)

    if @product_bundle.save
      render json: @product_bundle, status: :created, location: [:api, @product_bundle]
    else
      render json: @product_bundle.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /product_bundles/1
  api :PUT, "/product_bundles/:id", "Update product bundle with id :id"
  param :id, :number, desc: "ID of product", required: true
  param :product_bundle, Hash, desc: "Hash array with all params" do
    param :name, String, desc: "ID of client"
    param :price, :number, desc: "Price of product in grosze"
  end
  def update
    if @product_bundle.update(product_params)
      render json: @product_bundle
    else
      render json: @product_bundle.errors, status: :unprocessable_entity
    end
  end

  # DELETE /product_bundles/1
  api :DELETE, "/product_bundles/:id", "Delete product bundle with id :id"
  param :id, :number, desc: "ID of product", required: true
  def destroy
    @product_bundle.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_bundle
      @product_bundle = ProductBundle.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def product_params
      params.require(:product_bundle).permit(:name, :price)
    end
end
