class ProductCombination < ApplicationRecord
  belongs_to :unit, class_name: "ProductUnit"
  belongs_to :bundle, class_name: "ProductBundle"

  validates :unit, uniqueness: {scope: :bundle, message: "Can't add two same units to one bundle!"}
end
