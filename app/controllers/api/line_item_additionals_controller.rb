class Api::LineItemAdditionalsController < ApplicationController
  before_action :set_line_item_additional, only: [:show, :update, :destroy]

  # GET /line_item_additionals
  api :GET, "/line_item_additionals", "Show all additional line items"
  def index
    @line_item_additionals = LineItemAdditional.all

    render json: @line_item_additionals
  end

  # GET /line_item_additionals/1
  api :GET, "/line_item_additionals/:id", "Show additional line item with id :id"
  param :id, :number, desc: "ID of addtional line item", required: true
  def show
    render json: @line_item_additional
  end

  # POST /line_item_additionals
  api :POST, "/line_item_additionals", "Create new addtional line item"
  param :line_item_additional, Hash, desc: "Hash array with all params", required: true do
    param :line_item_id, :number, desc: "ID of line item", required: true
    param :product_id, :number, desc: "ID of product", required: true
  end
  def create
    @line_item_additional = LineItemAdditional.new(line_item_additional_params)

    if @line_item_additional.save
      render json: @line_item_additional, status: :created, location: [:api, @line_item_additional]
    else
      render json: @line_item_additional.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /line_item_additionals/1
  api :PUT, "/line_item_additionals/:id", "Update additional line item with id :id"
  param :id, :number, desc: "ID of additional line item", required: true
  param :line_item_additional, Hash, desc: "Hash array with all params", required: true do
    param :line_item_id, :number, desc: "ID of line item"
    param :product_id, :number, desc: "ID of product"
  end
  def update
    if @line_item_additional.update(line_item_additional_params)
      render json: @line_item_additional
    else
      render json: @line_item_additional.errors, status: :unprocessable_entity
    end
  end

  # DELETE /line_item_additionals/1
  api :DELETE, "/line_item_additionals/:id", "Delete addtional line item with id :id"
  param :id, :number, desc: "ID of addtional line item", required: true
  def destroy
    @line_item_additional.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_line_item_additional
      @line_item_additional = LineItemAdditional.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def line_item_additional_params
      params.require(:line_item_additional).permit(:line_item_id, :product_id)
    end
end
