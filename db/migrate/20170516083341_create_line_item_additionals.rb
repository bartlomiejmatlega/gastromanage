class CreateLineItemAdditionals < ActiveRecord::Migration[5.0]
  def change
    create_table :line_item_additionals do |t|
      t.references :line_item, foreign_key: true
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
