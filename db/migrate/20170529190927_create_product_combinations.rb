class CreateProductCombinations < ActiveRecord::Migration[5.0]
  def change
    create_table :product_combinations do |t|
      t.references :unit
      t.references :bundle

      t.timestamps
    end
  end
end
