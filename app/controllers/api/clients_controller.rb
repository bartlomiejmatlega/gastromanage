class Api::ClientsController < ApplicationController
  before_action :set_client, only: [:show, :update, :destroy]

  # GET /clients
  api :GET, "/clients", "Show all clients"
  def index
    @clients = Client.all

    render json: @clients
  end

  # GET /clients/1
  api :GET, "/clients/:id", "Show client with id :id"
  param :id, :number, desc: "ID of client", required: true
  def show
    render json: @client
  end

  # POST /clients
  api :POST, "/clients", "Create new client"
  param :client, Hash, desc: "Hash array with all params", required: true do
    param :first_name, String, desc: "First name", required: true
    param :last_name, String, desc: "Last name", required: true
    param :address, String, desc: "Address"
    param :zip_code, String, desc: "Zip code"
    param :city, String, desc: "City"
  end
  def create
    @client = Client.new(client_params)

    if @client.save
      render json: @client, status: :created, location: [:api, @client]
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /clients/1
  api :PUT, "/clients/:id", "Update client with id :id"
  param :id, :number, desc: "ID of client", required: true
  param :client, Hash, desc: "Hash array with all params", required: true do
    param :first_name, String, desc: "First name"
    param :last_name, String, desc: "Last name"
    param :address, String, desc: "Address"
    param :zip_code, String, desc: "Zip code"
    param :city, String, desc: "City"
  end
  def update
    if @client.update(client_params)
      render json: @client
    else
      render json: @client.errors, status: :unprocessable_entity
    end
  end

  # DELETE /clients/1
  api :DELETE, "/clients/:id", "Delete client with id :id"
  param :id, :number, desc: "ID of client", required: true
  def destroy
    @client.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def client_params
      params.require(:client).permit(:first_name, :last_name, :address, :zip_code, :city)
    end
end
